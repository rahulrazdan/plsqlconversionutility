package main;


public class Run {
	public static void main(String[] args) {
		
		AlphabetPattern.printCASPER();
		
		ToolParameters.readDBOTables();

		ToolParameters.readProperties();

		InputReader input = new InputReader();

		OutputWriter output = new OutputWriter();

		output.OutputFileWriter(input.InputFileReader());

	}
}
