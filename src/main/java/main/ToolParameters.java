package main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;


public class ToolParameters {
	private static Map<String,CustomModeClass> toolParameters;
	private static Map<String,String> dboTables;
	
	public static Map<String,String> getDboTables() {
		return dboTables;
	}

	public static Map<String, CustomModeClass> getToolParameters() {
		return toolParameters;
	}

	static void readProperties() {
		toolParameters = new LinkedHashMap<String,CustomModeClass>();
		try(FileInputStream file = new FileInputStream(new File("toolParameters.xlsx"))) {
			Properties prop = new Properties();
			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheetAt(0);
			
			for(int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
				StringBuilder key = new StringBuilder();
				StringBuilder value = new StringBuilder();
				StringBuilder patternToBeMatched = new StringBuilder();
				int noOfParameters = 0;
				StringBuilder outputFormat = new StringBuilder();
				DataFormatter formatter = new DataFormatter();
				for(int j = 0; j < 5; j++) {
					if(!formatter.formatCellValue(sheet.getRow(i).getCell(j)).equals("")) {
						if(j == 0)
							key = key.append(formatter.formatCellValue(sheet.getRow(i).getCell(j)));
						else if(j == 1)
							value = value.append(formatter.formatCellValue(sheet.getRow(i).getCell(j)));
						else if(j == 2)
							patternToBeMatched = patternToBeMatched.append(formatter.formatCellValue(sheet.getRow(i).getCell(j)));
						else if(j == 3)
							noOfParameters = Integer.parseInt(formatter.formatCellValue(sheet.getRow(i).getCell(j)));
						else if(j == 4)
							outputFormat = outputFormat.append(formatter.formatCellValue(sheet.getRow(i).getCell(j)));
					}
					toolParameters.put(key.toString(),new CustomModeClass(patternToBeMatched.toString(),noOfParameters,outputFormat.toString(),value.toString()));
					
				}
				if(!key.toString().equals("") && !value.toString().equals(""))
					prop.put(key.toString(), value.toString());
			}
			workbook.close();
			toolParameters.put("MODE", new CustomModeClass("",0,"","CUSTOM"));
			
		} catch (FileNotFoundException e) {
			toolParameters = new LinkedHashMap<String,CustomModeClass>();
			toolParameters.put("MODE", new CustomModeClass("",0,"","DEFAULT"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	static void readDBOTables() {
		dboTables = new HashMap<String,String>();
		try(CSVReader csvr = new CSVReader(new FileReader("PG_SQL_ETL_Tbl.csv"))){
			List<List<String>> records = new ArrayList<List<String>>();
			    String[] values = null;
			    boolean isHeader=true;
			    while ((values = csvr.readNext()) != null) {
			    	if(isHeader) {
			    		isHeader=false;
			    		continue;
			    	}
			        records.add(Arrays.asList(values));
			    }
			
			
			for(List<String> propertyName: records) { 
				if(propertyName.size()==2)
					dboTables.put(propertyName.get(0).toLowerCase(),propertyName.get(1));
			}
			
		}catch (FileNotFoundException e) {
			dboTables = new HashMap<String,String>();
			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
			try(InputStream is = classloader.getResourceAsStream("ListOfTables.properties")){
				Properties prop = new Properties();
				if(is!=null)
					prop.load(is);
				
				
				for(String propertyName: prop.stringPropertyNames()) 
					dboTables.put(propertyName.toLowerCase(),prop.getProperty(propertyName));
				
			} catch (IOException e1) {
				System.out.println("Error reading internal listOfTables.properties");
				e1.printStackTrace();
			}
			
		
		} catch (IOException e) {
			
			e.printStackTrace();
		} catch (CsvValidationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		try(CSVReader csvr = new CSVReader(new FileReader("PG_SQL_OLTP_Tbl.csv"))){
			List<List<String>> records = new ArrayList<List<String>>();
		    String[] values = null;
		    
		    while ((values = csvr.readNext()) != null) {
		        records.add(Arrays.asList(values));
		    }
		    
		    boolean isHeader=true;
		
			for(List<String> propertyName: records) { 
				if(isHeader) {
		    		isHeader=false;
		    		continue;
		    	}
				if(propertyName.size()==2)
					dboTables.put(propertyName.get(0).toLowerCase(),propertyName.get(1));
			}
			
		}catch (FileNotFoundException e) {
			dboTables = new HashMap<String,String>();
			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
			try(InputStream is = classloader.getResourceAsStream("ListOfTables.properties")){
				Properties prop = new Properties();
				if(is!=null)
					prop.load(is);
				
				for(String propertyName: prop.stringPropertyNames()) 
					dboTables.put(propertyName.toLowerCase(),prop.getProperty(propertyName));
				
			} catch (IOException e1) {
				System.out.println("Error reading internal listOfTables.properties");
				e1.printStackTrace();
			}
			
		
		} catch (IOException e) {
			
			e.printStackTrace();
		} catch (CsvValidationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
