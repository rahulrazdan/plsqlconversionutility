package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class AlphabetPattern {
	
	static void printCASPER()
	{
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		try(BufferedReader in = new BufferedReader(new InputStreamReader(classloader.getResourceAsStream("CASPER.txt")))){
			 
			String line = in.readLine();
			while(line != null)
			{
			  System.out.println(line);
			  line = in.readLine();
			}
			in.close();
			
		} catch (IOException e) {
			System.out.println("Alphabet Pattern file not found");
			e.printStackTrace();
		}
	}
}