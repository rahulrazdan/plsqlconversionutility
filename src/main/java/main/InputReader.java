package main;


import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InputReader {
	Map<String,String> InputFileReader() {
		Map<String,String> mapOfQueries = new HashMap<String,String>();
		List<File> inputFiles = listOfFiles("Input");
		
		for(File inputFile: inputFiles) {
	
			    BufferedReader reader = null;
				try {
					reader = new BufferedReader(new FileReader(inputFile));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			    String         line = null;
			    StringBuilder  stringBuilder = new StringBuilder();
			    String         ls = System.getProperty("line.separator");

			    try {
			        while((line = reader.readLine()) != null) {
			            stringBuilder.append(line);
			            stringBuilder.append(ls);
			        }
			    } catch (IOException e) {

					e.printStackTrace();
				} finally {
			        try {
						reader.close();
					} catch (IOException e) {

						e.printStackTrace();
					}
			    }
			   
				mapOfQueries.put(inputFile.getAbsolutePath(), stringBuilder.toString());
				
		
		}
		return mapOfQueries;
	}
	
	private List<File> listOfFiles(String directoryName){
		File directory = new File(directoryName);

        List<File> resultList = new ArrayList<File>();


        File[] fList = directory.listFiles();
        resultList.addAll(Arrays.asList(fList));
        for (File file : fList) {
            if (file.isDirectory()) {
                resultList.addAll(listOfFiles(file.getAbsolutePath()));
            }
        }

        return resultList;
	}
}
