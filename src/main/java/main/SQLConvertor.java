package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;


public class SQLConvertor {
	
	public String convertToPostgreSQL(String query, String fileName) {
		Map<String,CustomModeClass> parameters = ToolParameters.getToolParameters();
		StringBuffer sb = new StringBuffer(query);
		sb = this.replaceAtSymbol(sb);
		sb = new StringBuffer(this.addSchemaNameToTables(sb.toString())
				.replaceAll(Pattern.quote("["),"\"")
				.replaceAll(Pattern.quote("]"),"\""));
	
		if(parameters.get("MODE").getValue().equals("DEFAULT")) {
			
			sb = this.convertTempTable(sb);
			
			sb = this.replaceNoParameterFunctions(sb);

			sb = this.replaceConvertFunction(sb);

			sb = this.replaceStuffFunction(sb);

			sb = this.replaceDateDiffFunctionYear(sb);

			sb = this.replaceDateDiffFunctionDay(sb);
			
			sb = this.replaceSpaceFunction(sb);
			
			
			
		}
		else {	
		
			
			for(String function: parameters.keySet()) {

					if(!parameters.get(function).getValue().equals("CUSTOM") && !function.equals("MODE")  && !function.isEmpty())
						System.out.println("Converting "+function+" to "+parameters.get(function).getValue()+" for file- "+fileName);
					else if(parameters.get(function).getValue().equals("CUSTOM") && !function.equals("MODE")  && !function.isEmpty())
						System.out.println("Converting "+function+" to "+parameters.get(function).getOutputFormat()+" for file- "+fileName);
					switch(parameters.get(function).getValue())
			    	{
						
						
			    		case "OVERLAY":
			    			sb = this.replaceStuffFunction(sb);
			    			break;
			    		case "TO_<<datatype>>/CAST":
			    			sb = this.replaceConvertFunction(sb);
			    			break;
			    		case "EXTRACT":
			    			sb = this.replaceDateDiffFunctionYear(sb);
			    			sb = this.replaceDateDiffFunctionDay(sb);
			    			break;
			    		case "NOW":
			    			sb =  new StringBuffer(sb.toString().replaceAll("(?i)(?<=[(]|[\\s|\\s{2}|\\s{3}]|[,]|[+]|[-]|[*]|[)]|[\n]|[\r\n])"+Pattern.quote("getdate()"), "now()"));
			    			break;
			    		case "COALESCE":
			    			sb =  new StringBuffer(sb.toString().replaceAll("(?i)(?<=[(]|[\\s|\\s{2}|\\s{3}]|[,]|[+]|[-]|[*]|[)]|[\n]|[\r\n])"+Pattern.quote("isnull("), "COALESCE("));
			    			break;
			    		case "LENGTH":
			    			sb =  new StringBuffer(sb.toString().replaceAll("(?i)(?<=[(]|[\\s|\\s{2}|\\s{3}]|[,]|[+]|[-]|[*]|[)]|[\n]|[\r\n])"+Pattern.quote("len("), " LENGTH("));
			    			break;
			    
			    		case "TIMESTAMP":
			    			sb =  new StringBuffer(sb.toString().replaceAll("(?i)(?<=[(]|[\\s|\\s{2}|\\s{3}]|[,]|[+]|[-]|[*]|[)]|[\n]|[\r\n])"+Pattern.quote("DATETIME"),"TIMESTAMP"));
			    			break;
			    		case "VARCHAR":
			    			sb = new StringBuffer(sb.toString().replaceAll("(?i)(?<=[(]|[\\s|\\s{2}|\\s{3}]|[,]|[+]|[-]|[*]|[)]|[\n]|[\r\n])"+Pattern.quote("NVARCHAR"),"VARCHAR"));
			    			sb = new StringBuffer(sb.toString().replaceAll("(?i)(?<=[(]|[\\s|\\s{2}|\\s{3}]|[,]|[+]|[-]|[*]|[)]|[\n]|[\r\n])"+Pattern.quote("VARCHAR(MAX)"),"VARCHAR"));
			    			break;
			    		case "REPEAT":
			    			sb = this.replaceSpaceFunction(sb);
			    			break;
			    		case "PIPECONVERT FOR BOTH":
			    			sb = this.concatOperatorReplacementFunction(sb);
			    			sb = this.concatOperatorLIKEReplacementFunction(sb);
			    			break;
			    		case "PIPECONVERT FOR ONLY STRING LITERALS":
			    			sb=this.concatOperatorReplacementFunction(sb);
			    			break;
			    		case "PIPECONVERT FOR ONLY LIKE AND NOT LIKE":
			    			sb=this.concatOperatorLIKEReplacementFunction(sb);
			    			break;
			    		case "CUSTOM":
			    			sb = this.customReplaceFunction(sb,
			    					parameters.get(function).getPatternToBeMatched(),
			    					parameters.get(function).getNoOfParameters(),
			    					parameters.get(function).getOutputFormat());
			    			break;
			    		case "TEMP TABLE":
			    			sb = this.convertTempTable(sb);
			    			break;
			    		case "ILIKE":
			    			sb = new StringBuffer(sb.toString().replaceAll("(?i)(?<=[(]|[\\s|\\s{2}|\\s{3}]|[,]|[+]|[-]|[*]|[)]|[\n]|[\r\n])"+Pattern.quote("like "), "ILIKE "));
			    			break;
			    		
			    	}
					if(!parameters.get(function).getValue().equals("CUSTOM") && !function.equals("MODE") && !function.isEmpty())
						System.out.println("Conversion for "+function+" to "+parameters.get(function).getValue()+" is complete");
					else if(parameters.get(function).getValue().equals("CUSTOM") && !function.equals("MODE")  && !function.isEmpty())
						System.out.println("Conversion for "+function+" to "+parameters.get(function).getOutputFormat()+" is complete");
					System.out.println();
				
			}

		}
		return sb.toString();
	}
	
	protected StringBuffer replaceAtSymbol(StringBuffer query) {
		Pattern pattern = Pattern.compile(Pattern.quote("@"));
	    Matcher matcher = pattern.matcher(query);
	    
	    while(matcher.find()) {
	    	int i = matcher.start();
	    	if(i<query.length() && countChars('\'',query.substring(0, i).toString())%2==0) {
	    		query.replace(i, i+1, "");
	    		matcher = pattern.matcher(query);
	    	}
	    	else
	    		continue;
	    	
	    }
	    return query;
	}
	
	
	


	protected String addSchemaNameToTables(String query) {
		Map<String,String> tables = ToolParameters.getDboTables();
		
		for(String table: tables.keySet()) {
				query = query.replaceAll("(?i)(?<=[(]|[\\s|\\s{2}|\\s{3}]|,|\\[|[+])"+table+"(?![\\S])", tables.get(table)+"."+table);
		}
		return query.toString();
//		return "SET search_path = dbo, \"$user\", public;\n"+query;
	}
	
	protected StringBuffer replaceNoParameterFunctions(StringBuffer query) {

		query = new StringBuffer( query.toString().replaceAll("(?i)(?<=[(]|[\\s|\\s{2}|\\s{3}]|[,]|[+]|[-]|[*]|[)]|[\n]|[\r\n])"+Pattern.quote("getdate()"), "now()")
				.replaceAll("(?i)(?<=[(]|[\\s|\\s{2}|\\s{3}]|,|[+]|[-]|[*]|[\\]|[-]|[*]|[\\])"+Pattern.quote("isnull("), "COALESCE(")
				.replaceAll("(?i)(?<=[(]|[\\s|\\s{2}|\\s{3}]|[,]|[+]|[-]|[*]|[)]|[\n]|[\r\n])"+Pattern.quote("len("), " LENGTH(")
				.replaceAll("(?i)(?<=[(]|[\\s|\\s{2}|\\s{3}]|[,]|[+]|[-]|[*]|[)]|[\n]|[\r\n])"+Pattern.quote("like "), "ILIKE ")
				.replaceAll("(?i)(?<=[(]|[\\s|\\s{2}|\\s{3}]|[,]|[+]|[-]|[*]|[)]|[\n]|[\r\n])"+Pattern.quote("DATETIME"),"TIMESTAMP")
				.replaceAll("(?i)(?<=[(]|[\\s|\\s{2}|\\s{3}]|[,]|[+]|[-]|[*]|[)]|[\n]|[\r\n])"+Pattern.quote("NVARCHAR"),"VARCHAR")
				.replaceAll("(?i)(?<=[(]|[\\s|\\s{2}|\\s{3}]|[,]|[+]|[-]|[*]|[)]|[\n]|[\r\n])"+Pattern.quote("VARCHAR(MAX)"),"VARCHAR"));
		return query;
	}
	

	
	protected StringBuffer replaceStuffFunction(StringBuffer sb) {
		
		
		Pattern pattern = Pattern.compile("(?i)(?<=[(]|[\\s|\\s{2}|\\s{3}]|[,]|[+]|[-]|[*]|[)]|[\n]|[\r\n])"+Pattern.quote("stuff("));
	    Matcher matcher = pattern.matcher(sb);
	 
	    while (matcher.find()) {

	    	
	    	StringBuilder inputString = new StringBuilder();
	    	StringBuilder from = new StringBuilder();
	    	StringBuilder to = new StringBuilder();
	    	StringBuilder replaceBy = new StringBuilder();
	    	
	    	
	    	int noOfCommas = 0;
	    	int countOpenBrackets = 0;
	    	int i = matcher.end();
	    	
	    	for(; i < sb.length(); i++) {
	    	
	    		if(sb.charAt(i)==',' && countOpenBrackets == 0)
	    			noOfCommas++;
	    		
	    		else if(noOfCommas == 0 && sb.charAt(i)=='(') {
	    			inputString.append(sb.charAt(i));
	    			countOpenBrackets++;
	    		}
	    		else if(noOfCommas == 1 && sb.charAt(i)=='(') {
	    			from.append(sb.charAt(i));
	    			countOpenBrackets++;
	    		}
	    		else if(noOfCommas == 2 && sb.charAt(i)=='(') {
	    			to.append(sb.charAt(i));
	    			countOpenBrackets++;
	    		}
	    		else if(noOfCommas == 3 && sb.charAt(i)=='(') {
	    			replaceBy.append(sb.charAt(i));
	    			countOpenBrackets++;
	    		}
	    		
	    		else if(noOfCommas == 0 && sb.charAt(i)==')') {
	    			inputString.append(sb.charAt(i));
	    			countOpenBrackets--;
	    		}
	    		else if(noOfCommas == 1 && sb.charAt(i)==')') {
	    			from.append(sb.charAt(i));
	    			countOpenBrackets--;
	    		}
	    		else if(noOfCommas == 2 && sb.charAt(i)==')') {
	    			to.append(sb.charAt(i));
	    			countOpenBrackets--;
	    		}
	    		else if(noOfCommas == 3 && sb.charAt(i)==')') {
	    			if(countOpenBrackets>0)
	    				replaceBy.append(sb.charAt(i));
	    			countOpenBrackets--;
	    		}
	    		
	    		else if(noOfCommas==0 && sb.charAt(i)!=',' && countOpenBrackets==0) 
	    			inputString.append(sb.charAt(i));
	    		else if(noOfCommas==0 && countOpenBrackets>0) 
	    			inputString.append(sb.charAt(i));

	    		else if(noOfCommas==1 && sb.charAt(i)!=',' && countOpenBrackets==0)
	    			from.append(sb.charAt(i));
	    		else if(noOfCommas==1 && countOpenBrackets>0) 
	    			from.append(sb.charAt(i));
	    		
	    		else if(noOfCommas==2 && sb.charAt(i)!=',' && countOpenBrackets==0)
	    			to.append(sb.charAt(i));
	    		else if(noOfCommas==2 && countOpenBrackets>0) 
	    			to.append(sb.charAt(i));
	    		
	    		else if(noOfCommas==3 && sb.charAt(i)!=',' && countOpenBrackets==0)
	    			replaceBy.append(sb.charAt(i));
	    		else if(noOfCommas==3 && countOpenBrackets>0) 
	    			replaceBy.append(sb.charAt(i));
	    		
	    		if(noOfCommas==3 && countOpenBrackets==-1)
	    			break;
	    	}
	    	sb.replace(matcher.start(), i+1, "overlay("+inputString+"::varchar placing "+replaceBy+" from "+from+" for "+to+")");
	    	
	    }
		
		return sb;
		
		
		
	}
	
	protected StringBuffer replaceDateDiffFunctionYear(StringBuffer sb) {
		
		Pattern pattern = Pattern.compile("(?i)(?<=[(]|[\\s|\\s{2}|\\s{3}]|[,]|[+]|[-]|[*]|[)]|[\n]|[\r\n])"+Pattern.quote("datediff(year,"));
	    Matcher matcher = pattern.matcher(sb);

	    while (matcher.find()) {

	    	
	    	StringBuilder fromDate = new StringBuilder();
	    	StringBuilder toDate = new StringBuilder();
	    	
	    	int noOfCommas = 0;
	    	int countOpenBrackets = 0;

	    	int i = matcher.end();
	    	for(; i < sb.length(); i++) {

	    		if(sb.charAt(i)==',' && countOpenBrackets == 0)
	    			noOfCommas++;
	    		
	    		else if(noOfCommas == 0 && sb.charAt(i)=='(') {
	    			fromDate.append(sb.charAt(i));
	    			countOpenBrackets++;
	    		}
	    		else if(noOfCommas == 1 && sb.charAt(i)=='(') {
	    			toDate.append(sb.charAt(i));
	    			countOpenBrackets++;
	    		}
	    		
	    		else if(noOfCommas == 0 && sb.charAt(i)==')') {
	    			
	    			if(countOpenBrackets>0)
	    				fromDate.append(sb.charAt(i));
	    			countOpenBrackets--;
	    			
	    		}
	    		else if(noOfCommas == 1 && sb.charAt(i)==')') {
	    			
	    			if(countOpenBrackets>0)
	    				toDate.append(sb.charAt(i));
	    			countOpenBrackets--;
	    			
	    		}
	    		
	    		else if(noOfCommas==0 && sb.charAt(i)!=',' && countOpenBrackets==0) 
	    			fromDate.append(sb.charAt(i));
	    		else if(noOfCommas==0 && countOpenBrackets>0) 
	    			fromDate.append(sb.charAt(i));

	    		else if(noOfCommas==1 && sb.charAt(i)!=',' && countOpenBrackets==0)
	    			toDate.append(sb.charAt(i));
	    		else if(noOfCommas==1 && countOpenBrackets>0) 
	    			toDate.append(sb.charAt(i));
	    		
	    		
	    		if(noOfCommas==1 && countOpenBrackets==-1)
	    			break;
	    	}
	    	
	    	sb.replace(matcher.start(), i+1, "extract(year from age("+toDate+","+fromDate+"))");
	    	matcher = pattern.matcher(sb);
	    	
	    }
		
		return sb;
		
	}
	
	protected StringBuffer replaceDateDiffFunctionDay(StringBuffer sb) {
		
		Pattern pattern = Pattern.compile("(?i)(?<=[(]|[\\s|\\s{2}|\\s{3}]|[,]|[+]|[-]|[*]|[)]|[\n]|[\r\n])"+Pattern.quote("datediff(day,"));
	    Matcher matcher = pattern.matcher(sb);

	    while (matcher.find()) {

	    	
	    	StringBuilder fromDate = new StringBuilder();
	    	StringBuilder toDate = new StringBuilder();
	    	
	    	int noOfCommas = 0;
	    	int countOpenBrackets = 0;

	    	int i = matcher.end();
	    	for(; i < sb.length(); i++) {

	    		if(sb.charAt(i)==',' && countOpenBrackets == 0)
	    			noOfCommas++;
	    		
	    		else if(noOfCommas == 0 && sb.charAt(i)=='(') {
	    			fromDate.append(sb.charAt(i));
	    			countOpenBrackets++;
	    		}
	    		else if(noOfCommas == 1 && sb.charAt(i)=='(') {
	    			toDate.append(sb.charAt(i));
	    			countOpenBrackets++;
	    		}
	    		
	    		else if(noOfCommas == 0 && sb.charAt(i)==')') {
	    			
	    			if(countOpenBrackets>0)
	    				fromDate.append(sb.charAt(i));
	    			countOpenBrackets--;
	    			
	    		}
	    		else if(noOfCommas == 1 && sb.charAt(i)==')') {
	    			
	    			if(countOpenBrackets>0)
	    				toDate.append(sb.charAt(i));
	    			countOpenBrackets--;
	    			
	    		}
	    		
	    		else if(noOfCommas==0 && sb.charAt(i)!=',' && countOpenBrackets==0) 
	    			fromDate.append(sb.charAt(i));
	    		else if(noOfCommas==0 && countOpenBrackets>0) 
	    			fromDate.append(sb.charAt(i));

	    		else if(noOfCommas==1 && sb.charAt(i)!=',' && countOpenBrackets==0)
	    			toDate.append(sb.charAt(i));
	    		else if(noOfCommas==1 && countOpenBrackets>0) 
	    			toDate.append(sb.charAt(i));
	    		
	    		
	    		if(noOfCommas==1 && countOpenBrackets==-1)
	    			break;
	    	}
	    	
	    	sb.replace(matcher.start(), i+1, "extract(day from "+toDate+"::timestamp-"+fromDate+"::timestamp)");
	    	matcher = pattern.matcher(sb);
	    }
		
		return sb;
	}
	
	
	protected StringBuffer replaceSpaceFunction(StringBuffer sb) {
		Pattern pattern = Pattern.compile("(?i)(?<=[(]|[\\s|\\s{2}|\\s{3}]|[,]|[+]|[-]|[*]|[)]|[\n]|[\r\n])"+Pattern.quote("space(")+"");
	    Matcher matcher = pattern.matcher(sb);

	 
	 
	    while (matcher.find()) {

	    	
	    	StringBuilder number = new StringBuilder();
	    	
	    	int noOfCommas = 0;
	    	int countOpenBrackets = 0;

	    	int i = matcher.end();
	    	for(; i < sb.length(); i++) {

	    		if(sb.charAt(i)==',' && countOpenBrackets == 0)
	    			noOfCommas++;
	    		
	    		else if(noOfCommas == 0 && sb.charAt(i)=='(') {
	    			number.append(sb.charAt(i));
	    			countOpenBrackets++;
	    		}
	    		
	    		else if(noOfCommas == 0 && sb.charAt(i)==')') {
	    			
	    			if(countOpenBrackets>0)
	    				number.append(sb.charAt(i));
	    			countOpenBrackets--;
	    			
	    		}
	    		
	    		else if(noOfCommas==0 && sb.charAt(i)!=',' && countOpenBrackets==0) 
	    			number.append(sb.charAt(i));
	    		else if(noOfCommas==0 && countOpenBrackets>0) 
	    			number.append(sb.charAt(i));
	    		
	    		if(noOfCommas==0 && countOpenBrackets==-1)
	    			break;
	    	}
	    	
	    	sb.replace(matcher.start(), i+1, "repeat(\' \',"+number+")");
	    	matcher = pattern.matcher(sb);
	    	
	    }
		
		return sb;
		
	}
	
	
	
	protected StringBuffer replaceConvertFunction(StringBuffer query) {
		StringBuffer sb = new StringBuffer(query);
		Pattern pattern = Pattern.compile("(?i)(?<=[(]|[\\s|\\s{2}|\\s{3}]|[\\,]|[+]|[-]|[*]|[)])"+Pattern.quote("convert("));
	    Matcher matcher = pattern.matcher(sb);
	    while (matcher.find()) {

	    	
	    	StringBuilder datatype = new StringBuilder();
	    	StringBuilder givenString = new StringBuilder();
	    	StringBuilder styleCode = new StringBuilder();
	    	
	    	int noOfCommas = 0;
	    	int countOpenBrackets = 0;

	    	int i = matcher.end();
	    	for(; i < sb.length(); i++) {

	    		
	    		if(sb.charAt(i)==',' && countOpenBrackets == 0)
	    			noOfCommas++;
	    		
	    		else if(noOfCommas == 0 && sb.charAt(i)=='(') {
	    			datatype.append(sb.charAt(i));
	    			countOpenBrackets++;
	    		}
	    		else if(noOfCommas == 1 && sb.charAt(i)=='(') {
	    			givenString.append(sb.charAt(i));
	    			countOpenBrackets++;
	    		}
	    		else if(noOfCommas == 2 && sb.charAt(i)=='(') {
	    			styleCode.append(sb.charAt(i));
	    			countOpenBrackets++;
	    		}
	    		
	    		else if(noOfCommas == 0 && sb.charAt(i)==')') {
	    			
	    			if(countOpenBrackets>0)
	    				datatype.append(sb.charAt(i));
	    			countOpenBrackets--;
	    			
	    		}
	    		else if(noOfCommas == 1 && sb.charAt(i)==')') {
	    			
	    			if(countOpenBrackets>0)
	    				givenString.append(sb.charAt(i));
	    			countOpenBrackets--;
	    			
	    		}
	    		else if(noOfCommas == 2 && sb.charAt(i)==')') {
	    			
	    			if(countOpenBrackets>0)
	    				styleCode.append(sb.charAt(i));
	    			countOpenBrackets--;
	    			
	    		}
	    		
	    		else if(noOfCommas==0 && sb.charAt(i)!=',' && countOpenBrackets==0) 
	    			datatype.append(sb.charAt(i));
	    		else if(noOfCommas==0 && countOpenBrackets>0) 
	    			datatype.append(sb.charAt(i));

	    		else if(noOfCommas==1 && sb.charAt(i)!=',' && countOpenBrackets==0)
	    			givenString.append(sb.charAt(i));
	    		else if(noOfCommas==1 && countOpenBrackets>0) 
	    			givenString.append(sb.charAt(i));
	    		
	    		else if(noOfCommas==2 && sb.charAt(i)!=',' && countOpenBrackets==0)
	    			styleCode.append(sb.charAt(i));
	    		else if(noOfCommas==2 && countOpenBrackets>0) 
	    			styleCode.append(sb.charAt(i));
	    		
	    		
	    		
	    		if(noOfCommas==1 && countOpenBrackets==-1)
	    			break;
	    		else if(noOfCommas==2 && countOpenBrackets==-1)
	    			break;
	    	}
	    	if(noOfCommas==2) {
	    		Integer styleCodeInt = Integer.parseInt(styleCode.toString().trim());
	    		StringBuilder format = new StringBuilder();
	    	switch(styleCodeInt)
	    	{
	    		case 0:
	    			format.append("Mon dd yyyy hh:miPM");
	    			break;
	    		case 1:
	    			format.append("mm/dd/yy");
	    			break;
	    		case 2:
	    			format.append("yy.mm.dd");
	    			break;
	    		case 3:
	    			format.append("dd/mm/yy");
	    			break;
	    		case 4:
	    			format.append("dd.mm.yy");
	    			break;
	    		case 5:
	    			format.append("dd-mm-yy");
	    			break;
	    		case 6:
	    			format.append("dd Mon yy");
	    			break;
	    		case 7:
	    			format.append("Mon dd, yy");
	    			break;
	    		case 8:
	    			format.append("hh:mi:ss");
	    			break;
	    		case 9:
	    			format.append("Mon dd yyyy hh:mi:ss:msAM");
	    			break;
	    		case 10:
	    			format.append("mm-dd-yy");
	    			break;
	    		case 11:
	    			format.append("yy/mm/dd");
	    			break;
	    		case 12:
	    			format.append("yymmdd");
	    			break;
	    		case 13:
	    			format.append("dd mon yyyy hh24:mi:ss:ms");
	    			break;
	    		case 14:
	    			format.append("hh24:mi:ss:mmm");
	    			break;
	    		case 20:
	    			format.append("yyyy-mm-dd hh24:mi:ss");
	    			break;
	    		case 21:
	    			format.append("yyyy-mm-dd hh24:mi:ss.ms");
	    			break;
	    		case 100:
	    			format.append("Mon dd yyyy hh:miPM");
	    			break;
	    		case 101:
	    			format.append("mm/dd/yyyy");
	    			break;
	    		case 102:
	    			format.append("yyyy.mm.dd");
	    			break;
	    		case 103:
	    			format.append("dd/mm/yyyy");
	    			break;
	    		case 104:
	    			format.append("dd.mm.yyyy");
	    			break;
	    		case 105:
	    			format.append("dd-mm-yyyy");
	    			break;
	    		case 106:
	    			format.append("dd Mon yyyy");
	    			break;
	    		case 107:
	    			format.append("Mon dd, yyyy");
	    			break;
	    		case 108:
	    			format.append("hh:mi:ss");
	    			break;
	    		case 109:
	    			format.append("Mon dd yyyy hh:mi:ss:msAM");
	    			break;
	    		case 110:
	    			format.append("mm-dd-yyyy");
	    			break;
	    		case 111:
	    			format.append("yyyy/mm/dd");
	    			break;
	    		case 112:
	    			format.append("yyyymmdd");
	    			break;
	    		case 113:
	    			format.append("dd Mon yyyy hh24:mi:ss:ms");
	    			break;
	    		case 114:
	    			format.append("hh24:mi:ss:ms");
	    			break;
	    		case 120:
	    			format.append("yyyy-mm-dd hh24:mi:ss");
	    			break;
	    		case 121:
	    			format.append("yyyy-mm-dd hh24:mi:ss.ms");
	    			break;
	    		case 126:
	    			format.append("yyyy-mm-ddThh:mi:ss.ms");
	    			break;
	    		case 127:
	    			format.append("yyyy-mm-ddThh:mi:ss.msTZ");
	    			break;
	    		case 130:
	    			format.append("dd mon yyyy hh:mi:ss:msAM");
	    			break;
	    		case 131:
	    			format.append("dd/mm/yyyy hh:mi:ss:msAM");
	    			break;
	    		
	    		}
	    		if(datatype.toString().equalsIgnoreCase("datetime"))
	    			sb.replace(matcher.start(), i+1, "to_timestamp("+givenString+",'"+format+"')");
	    		else if(datatype.toString().equalsIgnoreCase("date"))
	    			sb.replace(matcher.start(), i+1, "to_date("+givenString+",'"+format+"')");
	    		else if(datatype.toString().equalsIgnoreCase("varchar"))
	    			sb.replace(matcher.start(), i+1, "to_char("+givenString+",'"+format+"')");
	    		else
	    			continue;
	    	}
	    	else {
	    		sb.replace(matcher.start(), i+1, "CAST("+givenString+" AS "+datatype+")");
	    		
	    	}
	    	matcher = pattern.matcher(sb);

	    }
		
		return sb;
	}
	
	protected StringBuffer customReplaceFunction(StringBuffer sb, String patternToMatch, int noOfParameters, String postgreSQLFunctionFormat) {
		
		if(noOfParameters == 0)
    		return new StringBuffer(sb.toString().replaceAll("(?i)(?<=[(]|[\\s|\\s{2}|\\s{3}]|[,]|[+]|[-]|[*]|[)]|[\n]|[\r\n])"+Pattern.quote(patternToMatch), postgreSQLFunctionFormat));
		Pattern pattern = Pattern.compile("(?i)(?<=[(]|[\\s|\\s{2}|\\s{3}]|[,]|[+]|[-]|[*]|[)]|[\n]|[\r\n])"+Pattern.quote(patternToMatch));
	    Matcher matcher = pattern.matcher(sb);


	    
	    while (matcher.find()) {
	    	
	    	List<StringBuilder> parameters = new ArrayList<StringBuilder>();
		    
		    for(int x = 0; x < noOfParameters; x++) 
		    	parameters.add(new StringBuilder(""));
	  
	    	int noOfCommas = 0;
	    	int countOpenBrackets = 0;

	    	int i = matcher.end();
	    	for(; i < sb.length(); i++) {

	    		if(sb.charAt(i)==',' && countOpenBrackets == 0)
	    			noOfCommas++;
	    		
	    		else if(sb.charAt(i)=='(') {
	    			parameters.set(noOfCommas, parameters.get(noOfCommas).append(sb.charAt(i)));
	    			countOpenBrackets++;
	    		}
	    		
	    		else if(sb.charAt(i)==')') {
	    			
	    			if(countOpenBrackets>0)
	    				parameters.set(noOfCommas, parameters.get(noOfCommas).append(sb.charAt(i)));
	    			countOpenBrackets--;
	    			
	    		}
	    		
	    		else if(sb.charAt(i)!=',' && countOpenBrackets==0) 
	    			parameters.set(noOfCommas, parameters.get(noOfCommas).append(sb.charAt(i)));
	    		else if(countOpenBrackets>0) 
	    			parameters.set(noOfCommas, parameters.get(noOfCommas).append(sb.charAt(i)));

	
	    		if(noOfCommas==noOfParameters-1 && countOpenBrackets==-1)
	    			break;
	    	}
	    	String postgreOutput = new String(postgreSQLFunctionFormat);
	    	for(int j = 0; j < noOfParameters; j++) 
	    		postgreOutput=("?????"+postgreOutput.replace("#"+(j+1), parameters.get(j).toString()));

	    	sb=sb.replace(matcher.start(), i+1, postgreOutput.toString());
	    	matcher = pattern.matcher(sb);
	    	
	    }
	    sb = new StringBuffer(sb.toString().replaceAll(Pattern.quote("?????"), ""));
		return sb;
	}
	
	
	protected StringBuffer concatOperatorReplacementFunction(StringBuffer strb) {
	
	    for(int i = 0; i < strb.length(); i++) {
	    	int start = i;
	    	int end = i;
	    	boolean isString = false;
	
	    	if(StringUtils.countMatches(strb.substring(0, i+1),"\'") %2 != 0) {
	    		continue;
	    	}
	    	if(strb.charAt(i)=='+') {
	    		
	    		start = i+1;
	    		ArrayList<String> listOfFields = new ArrayList<String>();
	    		StringBuilder str = new StringBuilder();
	    		boolean isOpenQuote=false;
	    		boolean prevCharIsPlus=true;
	    		boolean isOpenBracket=false;
	    		for(int j = i; j < strb.length(); j++) {
	    			
	    			
	    		
	    			if((strb.charAt(j)+"").matches("[+]") ) {
	    				if(isOpenQuote==false && isOpenBracket==false) {
		    				prevCharIsPlus = true;
		    			
		    				if(!str.toString().trim().isEmpty()) {
		    					listOfFields.add(str.toString());
		    					str = new StringBuilder();
		    				}
	    				}
	    				else {
	    					end=j;
	    					str.append(strb.charAt(j));
	    				}
	    			}
	    			else if((strb.charAt(j)+"").matches("[\\s]|[\n]|[\r\n]")) {
	    				end=j;
	    				str.append(strb.charAt(j));
	    				if((strb.charAt(j-1)+"").matches("[,]|[.]|[_]|[-]|[a-zA-Z ]|[0-9]") && isOpenQuote==false && isOpenBracket==false)
	    				{
	    					prevCharIsPlus=false;
	    					if((strb.charAt(j-1)+"").matches("[\\s]|[\n]|[\r\n]") && (strb.charAt(j-2)+"").matches("[,]|[.]|[_]|[-]|[a-zA-Z ]|[0-9]") && isOpenQuote==false && isOpenBracket==false) {
	    						prevCharIsPlus=false;
	    						if((strb.charAt(j-2)+"").matches("[\\s]|[\n]|[\r\n]") && (strb.charAt(j-3)+"").matches("[,]|[.]|[_]|[-]|[a-zA-Z ]|[0-9]") && isOpenQuote==false && isOpenBracket==false) {
		    						prevCharIsPlus=false;
		    					}
	    					}
	    				}
	    			}
	    			else if((strb.charAt(j)+"").matches("[,]|[.]|[_]|[-]|[a-zA-Z ]|[0-9]")) {
	    				
	    				if(prevCharIsPlus==false && isOpenQuote==false && isOpenBracket==false) {
	    					if(!str.toString().trim().isEmpty()) {
		    					listOfFields.add(str.toString());
		    					str = new StringBuilder();
		    				}
	    					end = j;
	    					break;
	    					
	    				}
	    				end=j;
	    				str.append(strb.charAt(j));
	  
	    			}
	    			else if((strb.charAt(j)+"").matches("[']")) {
	    				if(isOpenQuote)
	    					isOpenQuote=false;
	    				else
	    					isOpenQuote=true;
	    				isString=true;
	    				prevCharIsPlus = false;
	    				end=j;
	    				str.append(strb.charAt(j));
	    				if((j!=strb.length()-1 && isOpenQuote==false && isOpenBracket==false && strb.charAt(j+1)!= '\'')
	    						|| (j==strb.length()-1 && isOpenQuote==false && isOpenBracket==false)) {
	    					listOfFields.add(str.toString());
	    					str = new StringBuilder();
	    				}
	    			
	    			}
	    			else if((strb.charAt(j)+"").matches("[(]")) {
	    				if(isOpenQuote==false)
	    					isOpenBracket=true;
	    				prevCharIsPlus = false;
	    				end=j;
	    				str.append(strb.charAt(j));
	    			}
	    			else if((strb.charAt(j)+"").matches("[)]")) {
	    				if(isOpenQuote==false && isOpenBracket==false) {
	    					end=j;
	    					break;
	    				}
	    				if(isOpenQuote==false)
	    					isOpenBracket=false;
	    				prevCharIsPlus = false;
	    				end=j;
	    				str.append(strb.charAt(j));
	    			}
	    			else {
	    	
	    				if(isOpenQuote || isOpenBracket) {
	    					str.append(strb.charAt(j));
	    					end = j;
	    					continue;
	    				}
	    				if(prevCharIsPlus==false && isOpenQuote==false  && isOpenBracket==false) {
	    					
	    					if(!str.toString().trim().isEmpty()) {
	    						str.append(" ");
		    					listOfFields.add(str.toString());
		    					str = new StringBuilder();
		    				}
	    				}
	    				prevCharIsPlus = false;
	    				break;
	    			}
	    				
	    		}
				if(!str.toString().trim().isEmpty())
	    			listOfFields.add(str.toString());
		
	    		str = new StringBuilder();
	    		boolean nextBracketFunction=false;
	    		isOpenQuote=false;
	    		isOpenBracket=false;
	    		boolean hasBracket=false;
	    		for(int j = i; j>=0; j--){
	    			
	    			if((strb.charAt(j)+"").matches("[+]") ) {
	    				if(isOpenQuote==false  && isOpenBracket==false) {
	    					prevCharIsPlus = true;
	    			
	    					if(!str.toString().trim().isEmpty()) {
	    						listOfFields.add(0,str.reverse().toString());
	    						str = new StringBuilder();
	    					}
	    				}
	    				else {
	    					start=j;
	    					str.append(strb.charAt(j));
	    				}
	    			}
	    			else if((strb.charAt(j)+"").matches("[\\s]|[\n]|[\r\n]")) {
	    				str.append(strb.charAt(j));
	    				if((strb.charAt(j+1)+"").matches("[,]|[.]|[_]|[-]|[a-zA-Z ]|[0-9]") && isOpenQuote==false  && isOpenBracket==false)
	    				{
	    					prevCharIsPlus=false;
	    					hasBracket=false;
	    					nextBracketFunction=false;
	    					if((strb.charAt(j+1)+"").matches("[\\s]|[\n]|[\r\n]") && (strb.charAt(j+2)+"").matches("[,]|[.]|[_]|[-]|[a-zA-Z ]|[0-9]") && isOpenQuote==false  && isOpenBracket==false) {
	    						prevCharIsPlus=false;
	    						hasBracket=false;
		    					nextBracketFunction=false;
	    						if((strb.charAt(j+2)+"").matches("[\\s]|[\n]|[\r\n]") && (strb.charAt(j+3)+"").matches("[,]|[.]|[_]|[-]|[a-zA-Z ]|[0-9]") && isOpenQuote==false  && isOpenBracket==false) {
		    						prevCharIsPlus=false;
		    						hasBracket=false;
			    					nextBracketFunction=false;
		    					}
	    					}
	    					
	    				}
	    			}
	    			else if((strb.charAt(j)+"").matches("[,]|[.]|[_]|[-]|[a-zA-Z ]|[0-9]")) {
	    				if((strb.charAt(j)+"").matches("[a-zA-Z ]") && hasBracket && nextBracketFunction) {
	    					str.append(strb.charAt(j));
	    					start = j;
	    					prevCharIsPlus=false;
	    					continue;
	    				}
	    				if(prevCharIsPlus==false && isOpenQuote==false  && isOpenBracket==false) {
	    					if(!str.toString().trim().isEmpty()) {
		    					listOfFields.add(0,str.reverse().toString());
		    					str = new StringBuilder();
		    					nextBracketFunction=false;
		    					hasBracket=false;
		    					break;
		    				}
	    					break;
	    					
	    				}
	    				start = j;
	    				str.append(strb.charAt(j));
	  
	    			}
	    			else if((strb.charAt(j)+"").matches("[']")) {
	    				if(isOpenQuote)
	    					isOpenQuote=false;
	    				else
	    					isOpenQuote=true;
	    				isString=true;
	    				prevCharIsPlus = false;
	    				start = j;
	    				str.append(strb.charAt(j));
	    				if((j!=0 && isOpenQuote==false  && isOpenBracket==false && strb.charAt(j-1)!= '\'') || ( (j==0 && isOpenQuote==false && isOpenBracket==false))) {
	    					listOfFields.add(0,str.reverse().toString());
	    					str = new StringBuilder();
	    					nextBracketFunction=false;
	    					hasBracket=false;
	    				}
	    			}
	    			else if((strb.charAt(j)+"").matches("[(]")) {
	    				if(isOpenQuote==false && isOpenBracket==false) {
	    					break;
	    				}
	    				isOpenBracket=false;
	    				nextBracketFunction=true;
	    				prevCharIsPlus = false;
	    				start = j;
	    				str.append(strb.charAt(j));
	    			}
	    			else if((strb.charAt(j)+"").matches("[)]")) {
	    				isOpenBracket=true;
	    				hasBracket=true;
	    				prevCharIsPlus = false;
	    				start = j;
	    				str.append(strb.charAt(j));
	    			}
	    			else {
	    				
	    				
	    				if(isOpenQuote || isOpenBracket) {
	    					str.append(strb.charAt(j));
	    					start = j;
	    					continue;
	    				}
	    				if(prevCharIsPlus==false && isOpenQuote==false  && isOpenBracket==false) {
	    					
	    					if(!str.toString().trim().isEmpty()) {
	    						str.append(" ");
		    					listOfFields.add(0,str.reverse().toString());
		    					str = new StringBuilder();
		    					nextBracketFunction=false;
		    					hasBracket=false;
		    				}
	    				}
	    				prevCharIsPlus = false;
	    				break;
	    			}
	    		}
	    		if(!str.toString().trim().isEmpty())
	    			listOfFields.add(0,str.reverse().toString());
	    		i=end;
	    		
	    		if(isString) {
		    		StringBuilder replacement = new StringBuilder();
		    		for(int m = 0; m < listOfFields.size(); m++) {
		    			replacement.append(listOfFields.get(m));
		    			if(m!=listOfFields.size()-1)
		    				replacement.append(" || ");
		    			else if(m==0 || m==listOfFields.size()-1)
		    				replacement.append(" ");
		    		}
		    		if(end < strb.length() && start < strb.length())
		    			strb=strb.replace(start, end, replacement.toString());
	    		}
	    	}
	    }
		
		return strb;
	}
	
	protected StringBuffer concatOperatorLIKEReplacementFunction(StringBuffer strb) {
		
	

		Pattern pattern = Pattern.compile("(?i)(?<=[(]|[\\s|\\s{2}|\\s{3}]|[,]|[+]|[-]|[*]|[)]|[\n]|[\r\n])"+Pattern.quote("LIKE "));
	    Matcher matcher = pattern.matcher(strb);
	    
	    

	    while (matcher.find()) {
	    	
	    	boolean not=false;
	    	int start = matcher.start();
	    	int end = matcher.start();
	    	int match = matcher.start()-1;
	    	if(strb.substring(matcher.start()-4,matcher.start()-1).equalsIgnoreCase("NOT")) {
	    		match = matcher.start()-5;
	    		not=true;
	    	}
	    	else if(strb.substring(matcher.start()-5,matcher.start()-2).equalsIgnoreCase("NOT")){
	    		match = matcher.start()-6;
	    		not=true;
	    	}
	    	else if(strb.substring(matcher.start()-6,matcher.start()-3).equalsIgnoreCase("NOT")){
	    		match = matcher.start()-7;
	    		not=true;
	    	}
    		ArrayList<String> listOfFields = new ArrayList<String>();
    		StringBuilder str = new StringBuilder();
    		boolean isOpenQuote=false;
    		boolean prevCharIsPlus=true;
    		boolean isOpenBracket=false;
    		boolean nextBracketFunction=false;
    		boolean hasBracket=false;
    		boolean prevCharisOpenBracket=false;
    		for(int j = match; j>=0; j--){
    			

    			if((strb.charAt(j)+"").matches("[+]") && isOpenQuote==false  && isOpenBracket==false) {
    				prevCharIsPlus = true;
    				if(!str.toString().trim().isEmpty()) {
    					listOfFields.add(0,str.reverse().toString());
    					str = new StringBuilder();
    				}
    			}
    			else if((strb.charAt(j)+"").matches("[+]") && (isOpenQuote==true  || isOpenBracket==true)) {
    				if(isOpenQuote==true) {
    					start=j;
    					str.append(strb.charAt(j));
    				}
    				else {
    					prevCharIsPlus = true;
        				if(!str.toString().trim().isEmpty()) {
        					listOfFields.add(0,str.reverse().toString());
        					str = new StringBuilder();
        				}
    					
    				}
    			}
    			else if((strb.charAt(j)+"").matches("[\\s]|[\n]|[\r\n]")) {
    				start = j;
    				str.append(strb.charAt(j));
    				if(nextBracketFunction)
    					nextBracketFunction=false;
    				
    				if(not==true) {
    					if((strb.charAt(j+1)+"").matches("[,]|[.]|[_]|[-]|[a-zA-Z ]|[0-9]") && !strb.substring(j+1, j+4).equalsIgnoreCase("NOT") && isOpenQuote==false  && isOpenBracket==false)
        				{
        		
        					prevCharIsPlus=false;
        					hasBracket=false;
        					nextBracketFunction=false;
        					if((strb.charAt(j+1)+"").matches("[\\s]|[\n]|[\r\n]") && (strb.charAt(j+2)+"").matches("[,]|[.]|[_]|[-]|[a-zA-Z ]|[0-9]") && !strb.substring(j+2, j+5).equalsIgnoreCase("NOT") && isOpenQuote==false  && isOpenBracket==false) {
        						prevCharIsPlus=false;
        						hasBracket=false;
    	    					nextBracketFunction=false;
        						if((strb.charAt(j+2)+"").matches("[\\s]|[\n]|[\r\n]") && (strb.charAt(j+3)+"").matches("[,]|[.]|[_]|[-]|[a-zA-Z ]|[0-9]") && !strb.substring(j+3, j+6).equalsIgnoreCase("NOT") && isOpenQuote==false  && isOpenBracket==false) {
    	    						prevCharIsPlus=false;
    	    						hasBracket=false;
    		    					nextBracketFunction=false;
    	    					}
        					}
        					
        				}
    				}
    				else {
	    				if((strb.charAt(j+1)+"").matches("[,]|[.]|[_]|[-]|[a-zA-Z ]|[0-9]") && !strb.substring(j+1, j+5).equalsIgnoreCase("LIKE") && isOpenQuote==false  && isOpenBracket==false)
	    				{
	    		
	    					prevCharIsPlus=false;
	    					hasBracket=false;
	    					nextBracketFunction=false;
	    					if((strb.charAt(j+1)+"").matches("[\\s]|[\n]|[\r\n]") && (strb.charAt(j+2)+"").matches("[,]|[.]|[_]|[-]|[a-zA-Z ]|[0-9]") && !strb.substring(j+2, j+6).equalsIgnoreCase("LIKE") && isOpenQuote==false  && isOpenBracket==false) {
	    						prevCharIsPlus=false;
	    						hasBracket=false;
		    					nextBracketFunction=false;
	    						if((strb.charAt(j+2)+"").matches("[\\s]|[\n]|[\r\n]") && (strb.charAt(j+3)+"").matches("[,]|[.]|[_]|[-]|[a-zA-Z ]|[0-9]") && !strb.substring(j+3, j+7).equalsIgnoreCase("LIKE") && isOpenQuote==false  && isOpenBracket==false) {
		    						prevCharIsPlus=false;
		    						hasBracket=false;
			    					nextBracketFunction=false;
		    					}
	    					}
	    					
	    				}
	    			}
    			}
    			else if((strb.charAt(j)+"").matches("[,]|[.]|[_]|[-]|[a-zA-Z ]|[0-9]")) {
    				if((strb.charAt(j)+"").matches("[a-zA-Z ]") && hasBracket && nextBracketFunction) {
    					str.append(strb.charAt(j));
    					start = j;
    					prevCharIsPlus=false;
    					continue;
    				}
    				if(prevCharIsPlus==false && isOpenQuote==false  && isOpenBracket==false) {
    					if(!str.toString().trim().isEmpty()) {
	    					listOfFields.add(0,str.reverse().toString());
	    					str = new StringBuilder();
	    					nextBracketFunction=false;
	    					hasBracket=false;
	    					break;
	    				}
    					break;
    					
    				}
    				start = j;
    				str.append(strb.charAt(j));
  
    			}
    			else if((strb.charAt(j)+"").matches("[']")) {
    				if(isOpenQuote)
    					isOpenQuote=false;
    				else
    					isOpenQuote=true;
    	
    				prevCharIsPlus = false;
    				start = j;
    				str.append(strb.charAt(j));
    				if((j!=0 && isOpenQuote==false  && isOpenBracket==false && strb.charAt(j-1)!= '\'') || ( (j==0 && isOpenQuote==false && isOpenBracket==false))) {
    					listOfFields.add(0,str.reverse().toString());
    					str = new StringBuilder();
    					nextBracketFunction=false;
    					hasBracket=false;
    				}
    			}
    			else if((strb.charAt(j)+"").matches("[(]")) {
    				if(isOpenBracket==false && isOpenQuote==false) {
    					break;
    				}
    				if(isOpenQuote==false)
    					isOpenBracket=false;
    				nextBracketFunction=true;
    				prevCharIsPlus = false;
    				prevCharisOpenBracket=true;
    				start = j;
    				str.append(strb.charAt(j));
    			}
    			else if((strb.charAt(j)+"").matches("[)]")) {
    				if(isOpenQuote==false)
    					isOpenBracket=true;
    				hasBracket=true;
    				prevCharIsPlus = false;
    				start = j;
    				str.append(strb.charAt(j));
    			}
    			else {
    				if(prevCharisOpenBracket==true && isOpenQuote==false  && isOpenBracket==false) {
	    				prevCharIsPlus = false;
	    				if(isOpenQuote || isOpenBracket) {
	    					str.append(strb.charAt(j));
	    					start = j;
	    					continue;
	    				}
	    				else {
	    					break;
	    				}
    				}
    				if(prevCharIsPlus==false && isOpenQuote==false  && isOpenBracket==false) {
    					if(!str.toString().trim().isEmpty()) {
    						
    						str.append(" ");
	    					listOfFields.add(0,str.reverse().toString());
	    					str = new StringBuilder();
	    					nextBracketFunction=false;
	    					hasBracket=false;
	    				}
    				}
    				prevCharIsPlus = false;
    				break;
    			}
    		}
    		if(!str.toString().trim().isEmpty())
    			listOfFields.add(0,str.reverse().toString());
    
	    		StringBuilder replacement = new StringBuilder();
	    		for(int m = 0; m < listOfFields.size(); m++) {
	    			replacement.append(listOfFields.get(m));
	    			if(m!=listOfFields.size()-1)
	    				replacement.append(" || ");
	    			else if(m==0 || m==listOfFields.size()-1) {
	    				replacement.append(" ");
	    			}
	    		}
	    		if(end < strb.length() && start < strb.length()) {
	    			if(not==false)
	    				strb=strb.replace(start, matcher.end(), replacement.toString()+"ILIKE ");
	    			else
	    				strb=strb.replace(start, matcher.end(), replacement.toString()+"NOT ILIKE ");
	    		}
    		
    	}
    
	
		return strb;
	}
	
	protected StringBuffer convertTempTable(StringBuffer sb) {
		
		Pattern pattern = Pattern.compile("(?i)INTO[\\s]{1,3}\\#");
	    Matcher matcher = pattern.matcher(sb);
	    
	    while(matcher.find()) {
	    	StringBuilder sbuild= new StringBuilder();
	    	for(int i = matcher.start(); i>=matcher.start()-9; i--)
	    		sbuild.append(sb.charAt(i));
	    
	    	if(!sbuild.reverse().toString().trim().toLowerCase().contains("insert"))
	    		sb.replace(matcher.start(), matcher.end(), " INTO TEMP TABLE ");
	    }
	    
	    
	    pattern = Pattern.compile("(?i)CREATE[\\s]{1,3}(?i)TABLE[\\s]{1,3}\\#");
	    matcher = pattern.matcher(sb);
	    
	    while(matcher.find()) 
	    	sb.replace(matcher.start(), matcher.end(), " CREATE TEMP TABLE ");
	    
	    
	    sb= new StringBuffer(sb.toString().replaceAll("#", ""));
		return sb;
		
	}
	
	protected int countChars(char ch, String query) {
		Pattern pattern = Pattern.compile("["+ch+"]");
		Matcher matcher = pattern.matcher(query);
		int count = 0;
		while (matcher.find()) {
		    count++;
		}
		return count;
	}
	
}
