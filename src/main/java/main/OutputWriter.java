package main;

import java.io.File;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;

public class OutputWriter {
	void OutputFileWriter(Map<String,String> mapOfQueries) {
		File outputDirectory = new File("Output");
		outputDirectory.mkdir();
		SQLConvertor sqlConvertor = new SQLConvertor();

		for(String fileDir: mapOfQueries.keySet()) {
			
			try(FileOutputStream fos = new FileOutputStream(fileDir.replace("\\Input\\", "\\Output\\"));) {
				fos.write(sqlConvertor.convertToPostgreSQL(mapOfQueries.get(fileDir),fileDir).getBytes(Charset.forName("UTF-8")));

				fos.close();

			} catch (FileNotFoundException e) {

				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			}
			
		}
		

		
	}
}
